
    function animdown(block) {
        $(block).css({
            transform: "translateY(0%)",
            opacity: 1,  
            });
    }
    //-------------------------
   function animleft(block) {
        $(block).css({
             transform: "translateX(0%)",
             opacity: 1,  
            });
    }

function ready() {
    animleft(".content-anim");
    animdown(".anim-question");
    animdown(".anim-menu-foot");
    animdown(".anim-topmenu");

    $(".single-active:first").css("border-bottom","3px solid #6759ff");
}
document.addEventListener("DOMContentLoaded", ready);

var check = true;
$( ".question" ).click(function() {
    $(this).find(".box-answer-hint").slideToggle("slow");
  });


$(".single-active").click(function () {
    $(".single-active").css("color","#9ba5d1");
    $(".single-active").css("border-bottom","3px solid #071647");
    $(this).css("border-bottom","3px solid #6759ff");
    $(this).css("color","#6759ff");
    
});